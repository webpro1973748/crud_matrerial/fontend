import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import { ref } from 'vue'
import type { Matrerial } from '@/types/Matrerial'
import matrerialService from '@/services/matrerial'

export const useMatrerialStore = defineStore('matrerial', () => {
  const loadingStore = useLoadingStore()
  const matrerials = ref<Matrerial[]>([])

  async function getMatrerials() {
    loadingStore.doLoad()
    const res = await matrerialService.getMatrerials()
    matrerials.value = res.data
    loadingStore.finish()
  }
  async function getMatrerial(id: number) {
    loadingStore.doLoad()
    const res = await matrerialService.getMatrerial(id)
    matrerials.value = res.data
    loadingStore.finish()
  }

  async function saveMatrerial(matrerial: Matrerial) {
    loadingStore.doLoad()
    if (matrerial.id < 0) {
      // Add new
      console.log('post ' + JSON.stringify(matrerial))

      const res = await matrerialService.addMatrerial(matrerial)
    } else {
      // update
      console.log('patch ' + JSON.stringify(matrerial))
      const res = await matrerialService.updateMatrerial(matrerial)
    }
    await getMatrerials()
    loadingStore.finish()
  }
  async function deleteMatrerial(matrerial: Matrerial) {
    loadingStore.doLoad()
    const res = await matrerialService.delMatrerial(matrerial)
    await getMatrerials()
    loadingStore.finish()
  }
  return { matrerials, getMatrerials, deleteMatrerial, saveMatrerial, getMatrerial }
})
