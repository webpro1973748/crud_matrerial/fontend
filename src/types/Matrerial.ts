type Status = 'Available' | 'Low'
type Matrerial = {
  id: number
  name: string
  price: number
  quantity: number
  min: number
  status: Status
}

export type { Status, Matrerial }
