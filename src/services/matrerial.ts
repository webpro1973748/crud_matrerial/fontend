import type { Matrerial } from '@/types/Matrerial'
import http from './http'

function addMatrerial(matrerial: Matrerial) {
  return http.post('/matrerial', matrerial)
}

function updateMatrerial(matrerial: Matrerial) {
  return http.patch(`/matrerial/${matrerial.id}`, matrerial)
}

function delMatrerial(matrerial: Matrerial) {
  return http.delete(`/matrerial/${matrerial.id}`)
}

function getMatrerial(id: number) {
  return http.get(`/matrerial/${id}`)
}

function getMatrerials() {
  return http.get('/matrerial')
}

export default { addMatrerial, delMatrerial, updateMatrerial, getMatrerial, getMatrerials }
